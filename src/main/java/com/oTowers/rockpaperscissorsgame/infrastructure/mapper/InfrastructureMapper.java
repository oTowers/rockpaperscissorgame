package com.oTowers.rockpaperscissorsgame.infrastructure.mapper;

import com.oTowers.rockpaperscissorsgame.domain.model.game.GameReport;
import com.oTowers.rockpaperscissorsgame.domain.model.game.GameResultsContext;
import com.oTowers.rockpaperscissorsgame.infrastructure.model.GameReportInfrastructure;
import com.oTowers.rockpaperscissorsgame.infrastructure.model.entities.PlayerGameResults;
import org.springframework.stereotype.Component;

@Component
public class InfrastructureMapper {

    public PlayerGameResults gameResultsContextToPlayerGameResults(GameResultsContext gameResults) {

        PlayerGameResults playerGameResults = new PlayerGameResults();

        playerGameResults.setCounterWonPlayerOne(gameResults.getCounterWonPlayerOne());
        playerGameResults.setCounterWonPlayerTwo(gameResults.getCounterWonPlayerTwo());
        playerGameResults.setCounterDraw(gameResults.getCounterDraw());
        playerGameResults.setCounterRoundsPlayed(gameResults.getCounterRoundsPlayed());

        return playerGameResults;
    }

    public GameReport gameReportInfrastructureTorGameReport(GameReportInfrastructure gameReportInfrastructure) {

        GameReport gameReport = new GameReport();

        gameReport.setTotalWonPlayerOne(gameReportInfrastructure.getTotalWonPlayerOne());
        gameReport.setTotalWonPlayerTwo(gameReportInfrastructure.getTotalWonPlayerTwo());
        gameReport.setTotalDraw(gameReportInfrastructure.getTotalDraw());
        gameReport.setTotalRoundsPlayed(gameReportInfrastructure.getTotalRoundsPlayed());

        return gameReport;
    }

}
