package com.oTowers.rockpaperscissorsgame.infrastructure.model.repository;

import com.oTowers.rockpaperscissorsgame.infrastructure.model.entities.PlayerGameResults;

import java.util.List;

public interface PlayerGameResultsRepository {

    PlayerGameResults findByPlayerId(String playerId);

    PlayerGameResults save(String playerId, PlayerGameResults gameResults);

    Boolean isRegister(String playerId);

    List<PlayerGameResults> findAll();
}
