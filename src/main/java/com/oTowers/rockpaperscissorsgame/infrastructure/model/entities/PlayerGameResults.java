package com.oTowers.rockpaperscissorsgame.infrastructure.model.entities;

import lombok.Data;

@Data
public class PlayerGameResults {

    private String playerId;
    private Integer counterWonPlayerOne = 0;
    private Integer counterWonPlayerTwo = 0;
    private Integer counterDraw = 0;
    private Integer counterRoundsPlayed = 0;

}
