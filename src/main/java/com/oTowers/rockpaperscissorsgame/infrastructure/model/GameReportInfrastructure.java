package com.oTowers.rockpaperscissorsgame.infrastructure.model;

import lombok.Data;

@Data
public class GameReportInfrastructure {

    private Integer totalWonPlayerOne = 0;
    private Integer totalWonPlayerTwo = 0;
    private Integer totalDraw = 0;
    private Integer totalRoundsPlayed = 0;

}
