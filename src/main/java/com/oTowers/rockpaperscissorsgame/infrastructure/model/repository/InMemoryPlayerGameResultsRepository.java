package com.oTowers.rockpaperscissorsgame.infrastructure.model.repository;

import com.oTowers.rockpaperscissorsgame.infrastructure.model.entities.PlayerGameResults;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class InMemoryPlayerGameResultsRepository implements PlayerGameResultsRepository {

    private final Map<String, PlayerGameResults> memoryGameResultsMap = new HashMap<>();

    @Override
    public PlayerGameResults findByPlayerId(String playerId) {
        return memoryGameResultsMap.containsKey(playerId) ? memoryGameResultsMap.get(playerId) : new PlayerGameResults();
    }

    @Override
    public PlayerGameResults save(String playerId, PlayerGameResults gameResults) {
        return memoryGameResultsMap.put(playerId, gameResults);
    }

    @Override
    public Boolean isRegister(String playerId) {
        return memoryGameResultsMap.containsKey(playerId);
    }

    @Override
    public List<PlayerGameResults> findAll() {
        return new ArrayList<>(memoryGameResultsMap.values());
    }
}
