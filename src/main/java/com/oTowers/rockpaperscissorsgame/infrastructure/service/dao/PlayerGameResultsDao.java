package com.oTowers.rockpaperscissorsgame.infrastructure.service.dao;

import com.oTowers.rockpaperscissorsgame.infrastructure.model.GameReportInfrastructure;
import com.oTowers.rockpaperscissorsgame.infrastructure.model.entities.PlayerGameResults;
import com.oTowers.rockpaperscissorsgame.infrastructure.model.repository.PlayerGameResultsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PlayerGameResultsDao {

    @Autowired
    PlayerGameResultsRepository playerGameResultsRepository;

    public PlayerGameResults findByPlayerId(String playerId) {
        return playerGameResultsRepository.isRegister(playerId) ?
                playerGameResultsRepository.findByPlayerId(playerId) : new PlayerGameResults();
    }

    public PlayerGameResults save(String playerId, PlayerGameResults gameResults) {
        return playerGameResultsRepository.save(playerId, gameResults);
    }

    public GameReportInfrastructure retrieveReport() {
        GameReportInfrastructure gameReport = new GameReportInfrastructure();
        List<PlayerGameResults> allPlayerGameResult = playerGameResultsRepository.findAll();

        gameReport.setTotalWonPlayerOne(allPlayerGameResult.stream()
                .mapToInt(PlayerGameResults::getCounterWonPlayerOne).sum());
        gameReport.setTotalWonPlayerTwo(allPlayerGameResult.stream()
                .mapToInt(PlayerGameResults::getCounterWonPlayerTwo).sum());
        gameReport.setTotalDraw(allPlayerGameResult.stream()
                .mapToInt(PlayerGameResults::getCounterDraw).sum());
        gameReport.setTotalRoundsPlayed(allPlayerGameResult.stream()
                .mapToInt(PlayerGameResults::getCounterRoundsPlayed).sum());

        return gameReport;
    }

}
