package com.oTowers.rockpaperscissorsgame.domain.model.game;

import com.oTowers.rockpaperscissorsgame.domain.model.HandDecisions;
import com.oTowers.rockpaperscissorsgame.domain.model.HandStrategy;
import com.oTowers.rockpaperscissorsgame.domain.model.rounds.RoundResults;
import com.oTowers.rockpaperscissorsgame.infrastructure.model.entities.PlayerGameResults;
import lombok.Data;

@Data
public class GameResultsContext {

    private Integer counterWonPlayerOne = 0;
    private Integer counterWonPlayerTwo = 0;
    private Integer counterDraw = 0;
    private Integer counterRoundsPlayed = 0;
    private RoundResults currentRoundResult;
    private HandDecisions currentHandDecisionPlayerOne;
    private HandDecisions currentHandDecisionPlayerTwo;

    public void setPLayerGameResults(PlayerGameResults playerGameResults) {
        counterWonPlayerOne = playerGameResults.getCounterWonPlayerOne();
        counterWonPlayerTwo = playerGameResults.getCounterWonPlayerTwo();
        counterDraw = playerGameResults.getCounterDraw();
        counterRoundsPlayed = playerGameResults.getCounterRoundsPlayed();
    }

    public void setPlayerHandsDecisions(HandDecisions handDecisionPlayerOne, HandDecisions handDecisionPlayerTwo) {
        this.currentHandDecisionPlayerOne = handDecisionPlayerOne;
        this.currentHandDecisionPlayerTwo = handDecisionPlayerTwo;
    }


    public void addRoundResult(RoundResults roundResult) {
        currentRoundResult = roundResult;
        counterRoundsPlayed++;
        switch (roundResult) {
            case DRAW:
                counterDraw++;
                break;
            case PLAYER_ONE_WIN:
                counterWonPlayerOne++;
                break;
            case PLAYER_TWO_WIN:
                counterWonPlayerTwo++;
                break;
        }
    }

}
