package com.oTowers.rockpaperscissorsgame.domain.model.game;

import lombok.Data;

@Data
public class GameReport {

    private Integer totalWonPlayerOne = 0;
    private Integer totalWonPlayerTwo = 0;
    private Integer totalDraw = 0;
    private Integer totalRoundsPlayed = 0;

}
