package com.oTowers.rockpaperscissorsgame.domain.model;

public class PaperHandStrategy implements HandStrategy {

    @Override
    public HandDecisions getDecision() {
        return HandDecisions.PAPER;
    }

}
