package com.oTowers.rockpaperscissorsgame.domain.model.rounds;

public enum RoundResults {

    DRAW,
    PLAYER_ONE_WIN,
    PLAYER_TWO_WIN

}
