package com.oTowers.rockpaperscissorsgame.domain.model.players;

import com.oTowers.rockpaperscissorsgame.domain.model.HandStrategiesFactory;
import com.oTowers.rockpaperscissorsgame.domain.model.HandStrategiesType;
import com.oTowers.rockpaperscissorsgame.domain.model.HandStrategy;

public class RockStrategyPlayer implements Player {

    @Override
    public HandStrategy getStrategyDecision() {
        HandStrategiesFactory handStrategiesFactory = new HandStrategiesFactory();
        return handStrategiesFactory.getHandStrategy(HandStrategiesType.ROCK);
    }

}
