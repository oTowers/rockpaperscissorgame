package com.oTowers.rockpaperscissorsgame.domain.model;

public class HandStrategiesFactory {

    public HandStrategy getHandStrategy(HandStrategiesType strategyType) {
        HandStrategy handStrategy = null;
        switch (strategyType) {
            case ROCK:
                handStrategy = new RockHandStrategy();
                break;
            case PAPER:
                handStrategy = new PaperHandStrategy();
                break;
            case SCISSORS:
                handStrategy = new ScissorsHandStrategy();
                break;
            case RANDOM:
                handStrategy = new RandomHandStrategy();
                break;
        }
        return handStrategy;
    }

}
