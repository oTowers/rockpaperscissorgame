package com.oTowers.rockpaperscissorsgame.domain.model;

public class RockHandStrategy implements HandStrategy {

    @Override
    public HandDecisions getDecision() {
        return HandDecisions.ROCK;
    }

}
