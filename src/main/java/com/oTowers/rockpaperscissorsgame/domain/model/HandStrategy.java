package com.oTowers.rockpaperscissorsgame.domain.model;

public interface HandStrategy {

    HandDecisions getDecision();

}
