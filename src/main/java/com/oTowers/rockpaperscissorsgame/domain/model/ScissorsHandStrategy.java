package com.oTowers.rockpaperscissorsgame.domain.model;

public class ScissorsHandStrategy implements HandStrategy {

    @Override
    public HandDecisions getDecision() {
        return HandDecisions.SCISSORS;
    }

}
