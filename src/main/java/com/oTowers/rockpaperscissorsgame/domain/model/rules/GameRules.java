package com.oTowers.rockpaperscissorsgame.domain.model.rules;

import com.oTowers.rockpaperscissorsgame.domain.model.HandDecisions;
import com.oTowers.rockpaperscissorsgame.domain.model.rounds.RoundResults;

public interface GameRules {

    RoundResults obtainWinner(HandDecisions playerOneDecision, HandDecisions playerTwoDecision);

}
