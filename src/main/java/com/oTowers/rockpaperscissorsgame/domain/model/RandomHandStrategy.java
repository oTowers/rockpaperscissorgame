package com.oTowers.rockpaperscissorsgame.domain.model;

import java.util.Random;

public class RandomHandStrategy implements HandStrategy {

    @Override
    public HandDecisions getDecision() {
        return generateRandomHandDecision();
    }

    private HandDecisions generateRandomHandDecision() {
        HandDecisions[] values = HandDecisions.values();
        int length = values.length;
        int randIndex = new Random().nextInt(length);
        return values[randIndex];
    }

}
