package com.oTowers.rockpaperscissorsgame.domain.model.players;

import com.oTowers.rockpaperscissorsgame.domain.model.HandStrategy;

public interface Player {

    HandStrategy getStrategyDecision();

}
