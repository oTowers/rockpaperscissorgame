package com.oTowers.rockpaperscissorsgame.domain.model;

public enum HandDecisions {

    ROCK,
    PAPER,
    SCISSORS

}
