package com.oTowers.rockpaperscissorsgame.domain.model.rules;

import com.oTowers.rockpaperscissorsgame.domain.model.HandDecisions;
import com.oTowers.rockpaperscissorsgame.domain.model.rounds.RoundResults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class RockPaperScissorsRules implements GameRules {

    private final Map<HandDecisions, HandDecisions> RULES = new HashMap<>();
    {
        RULES.put(HandDecisions.SCISSORS, HandDecisions.PAPER);
        RULES.put(HandDecisions.PAPER, HandDecisions.ROCK);
        RULES.put(HandDecisions.ROCK, HandDecisions.SCISSORS);
    }

    @Override
    public RoundResults obtainWinner(HandDecisions playerOneDecision, HandDecisions playerTwoDecision) {
        RoundResults roundResult;
        if (playerOneDecision.equals(playerTwoDecision)) {
            roundResult = RoundResults.DRAW;
        } else {
            if (RULES.get(playerOneDecision).equals(playerTwoDecision)) {
                roundResult = RoundResults.PLAYER_ONE_WIN;
            } else {
                roundResult = RoundResults.PLAYER_TWO_WIN;
            }
        }
        log.info("Executing rules, player one decision: " + playerOneDecision +
                ", player two decision: " + playerTwoDecision + " => " + roundResult);
        return roundResult;
    }

}
