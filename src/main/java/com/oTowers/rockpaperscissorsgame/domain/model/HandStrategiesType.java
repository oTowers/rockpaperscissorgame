package com.oTowers.rockpaperscissorsgame.domain.model;

public enum HandStrategiesType {
    ROCK,
    PAPER,
    SCISSORS,
    RANDOM
}
