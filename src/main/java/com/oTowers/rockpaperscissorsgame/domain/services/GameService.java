package com.oTowers.rockpaperscissorsgame.domain.services;

import com.oTowers.rockpaperscissorsgame.domain.model.HandDecisions;
import com.oTowers.rockpaperscissorsgame.domain.model.game.GameReport;
import com.oTowers.rockpaperscissorsgame.domain.model.game.GameResultsContext;
import com.oTowers.rockpaperscissorsgame.domain.model.players.Player;
import com.oTowers.rockpaperscissorsgame.domain.model.players.RandomStrategyPlayer;
import com.oTowers.rockpaperscissorsgame.domain.model.players.RockStrategyPlayer;
import com.oTowers.rockpaperscissorsgame.domain.model.rules.GameRules;
import com.oTowers.rockpaperscissorsgame.infrastructure.mapper.InfrastructureMapper;
import com.oTowers.rockpaperscissorsgame.infrastructure.model.entities.PlayerGameResults;
import com.oTowers.rockpaperscissorsgame.infrastructure.service.dao.PlayerGameResultsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GameService {

    @Autowired
    PlayerGameResultsDao playerGameResultsDao;

    @Autowired
    GameRules rockPaperScissorsRules;

    @Autowired
    InfrastructureMapper infrastructureMapper;

    public GameResultsContext playGame(String playerId) {

        PlayerGameResults playerGameResults = playerGameResultsDao.findByPlayerId(playerId);

        GameResultsContext gameResults = playRound(playerGameResults);

        playerGameResultsDao.save(playerId, infrastructureMapper.gameResultsContextToPlayerGameResults(gameResults));

        return gameResults;

    }

    public GameReport getReport() {
        return infrastructureMapper.gameReportInfrastructureTorGameReport(playerGameResultsDao.retrieveReport());
    }

    private GameResultsContext playRound(PlayerGameResults playerGameResults) {
        GameResultsContext gameResults = new GameResultsContext();
        gameResults.setPLayerGameResults(playerGameResults);

        Player playerOne = new RandomStrategyPlayer();
        HandDecisions handDecisionsPlayerOne = playerOne.getStrategyDecision().getDecision();

        Player playerTwo = new RockStrategyPlayer();
        HandDecisions handDecisionsPlayerTwo = playerTwo.getStrategyDecision().getDecision();

        gameResults.setPlayerHandsDecisions(handDecisionsPlayerOne, handDecisionsPlayerTwo);

        gameResults.addRoundResult(rockPaperScissorsRules.obtainWinner(handDecisionsPlayerOne, handDecisionsPlayerTwo));

        return gameResults;
    }
}
