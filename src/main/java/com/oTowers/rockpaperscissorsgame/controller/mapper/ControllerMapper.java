package com.oTowers.rockpaperscissorsgame.controller.mapper;

import com.oTowers.rockpaperscissorsgame.controller.dto.GameDto;
import com.oTowers.rockpaperscissorsgame.controller.dto.GameReportDto;
import com.oTowers.rockpaperscissorsgame.domain.model.game.GameReport;
import com.oTowers.rockpaperscissorsgame.domain.model.game.GameResultsContext;
import org.springframework.stereotype.Component;

@Component
public class ControllerMapper {

    public GameDto gameResultContextToGameDto(GameResultsContext gameResultsContext) {
        GameDto gameStatus = new GameDto();
        gameStatus.setPlayerOneChose(gameResultsContext.getCurrentHandDecisionPlayerOne().toString());
        gameStatus.setPlayerTwoChose(gameResultsContext.getCurrentHandDecisionPlayerTwo().toString());
        gameStatus.setRoundResult(gameResultsContext.getCurrentRoundResult().toString());
        gameStatus.setRoundsPlayed(gameResultsContext.getCounterRoundsPlayed());
        return gameStatus;
    }

    public GameReportDto gamesReportToGamesReportDto(GameReport gameReport) {
        GameReportDto gameReportDto = new GameReportDto();
        gameReportDto.setTotalWonPlayerOne(gameReport.getTotalWonPlayerOne());
        gameReportDto.setTotalWonPlayerTwo(gameReport.getTotalWonPlayerTwo());
        gameReportDto.setTotalDraw(gameReport.getTotalDraw());
        gameReportDto.setTotalRoundsPlayed(gameReport.getTotalRoundsPlayed());
        return gameReportDto;
    }
}
