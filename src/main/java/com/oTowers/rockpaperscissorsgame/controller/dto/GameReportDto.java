package com.oTowers.rockpaperscissorsgame.controller.dto;

import lombok.Data;

@Data
public class GameReportDto {

    private Integer totalWonPlayerOne = 0;
    private Integer totalWonPlayerTwo = 0;
    private Integer totalDraw = 0;
    private Integer totalRoundsPlayed = 0;

}
