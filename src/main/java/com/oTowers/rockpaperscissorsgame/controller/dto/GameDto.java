package com.oTowers.rockpaperscissorsgame.controller.dto;

import lombok.Data;

@Data
public class GameDto {

    private String userUUID;
    private Integer roundsPlayed;
    private String playerOneChose;
    private String playerTwoChose;
    private String roundResult;

}
