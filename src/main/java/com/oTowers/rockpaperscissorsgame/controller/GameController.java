package com.oTowers.rockpaperscissorsgame.controller;

import com.oTowers.rockpaperscissorsgame.controller.dto.GameDto;
import com.oTowers.rockpaperscissorsgame.controller.mapper.ControllerMapper;
import com.oTowers.rockpaperscissorsgame.domain.services.GameService;
import com.oTowers.rockpaperscissorsgame.domain.model.game.GameResultsContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@Slf4j
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private ControllerMapper controllerMapper;

    @RequestMapping("/")
    public String showGame(Model modelo) {

        UUID uuid = UUID.randomUUID();
        log.info("UUID generated: " + uuid);

        modelo.addAttribute("userUUID", uuid);
        modelo.addAttribute("gameStatus", new GameDto());
        return "game";
    }

    @PostMapping("/playGame")
    public String playGame(Model modelo, @ModelAttribute(value = "userUUID") String uuid) {

        String playerUuid = generateIfNotExistUUID(uuid);
        log.info("UUID: " + uuid + ", playing");

        GameResultsContext gameResultsContext = gameService.playGame(playerUuid);

        modelo.addAttribute("userUUID", playerUuid);
        modelo.addAttribute("gameStatus", controllerMapper.gameResultContextToGameDto(gameResultsContext));
        return "game";
    }

    @PostMapping("/resetGame")
    public String resetGame(Model modelo) {

        log.info("game reset");
        UUID newPlayerUuid = UUID.randomUUID();

        modelo.addAttribute("userUUID", newPlayerUuid);
        modelo.addAttribute("gameStatus", new GameDto());
        return "game";
    }

    @RequestMapping("/report")
    public String getReport(Model modelo) {

        modelo.addAttribute("gamesReport", controllerMapper
                .gamesReportToGamesReportDto(gameService.getReport()));
        return "report";
    }

    private String generateIfNotExistUUID(String uuid) {
        return (uuid == null || uuid.isEmpty()) ? UUID.randomUUID().toString() : uuid;
    }
}
