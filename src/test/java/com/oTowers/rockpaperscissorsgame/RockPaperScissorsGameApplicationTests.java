package com.oTowers.rockpaperscissorsgame;

import com.oTowers.rockpaperscissorsgame.domain.model.game.GameReport;
import com.oTowers.rockpaperscissorsgame.domain.model.game.GameResultsContext;
import com.oTowers.rockpaperscissorsgame.domain.services.GameService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RockPaperScissorsGameApplicationTests {

	@Autowired
	private GameService gameService;

	@Test
	void playSeveralRoundsForDifferentUsersAndGetExpectedResult() {
		String playerIdOne = "123"; // 5
		gameService.playGame(playerIdOne);
		gameService.playGame(playerIdOne);
		gameService.playGame(playerIdOne);
		gameService.playGame(playerIdOne);
		gameService.playGame(playerIdOne);

		String playerIdTwo = "456"; // 3
		gameService.playGame(playerIdTwo);
		gameService.playGame(playerIdTwo);
		gameService.playGame(playerIdTwo);

		String playerIdTree = "789"; // 2
		gameService.playGame(playerIdTree);
		gameService.playGame(playerIdTree);

		GameReport gameReport = gameService.getReport();

		Assertions.assertThat(gameReport.getTotalRoundsPlayed()).isEqualTo(10); // 5 + 3 + 2
	}

}
