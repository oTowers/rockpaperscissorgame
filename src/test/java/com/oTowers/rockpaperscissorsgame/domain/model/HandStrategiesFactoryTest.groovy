package com.oTowers.rockpaperscissorsgame.domain.model

import spock.lang.Specification
import spock.lang.Unroll

class HandStrategiesFactoryTest extends Specification {

    HandStrategiesFactory handStrategiesFactory

    def setup() {
        handStrategiesFactory = new HandStrategiesFactory()
    }

    @Unroll
    def "Get Hand Strategy depends on Hand Strategy Type"() {
        expect:
        HandStrategy handStrategy = handStrategiesFactory.getHandStrategy(handStrategyType)
        classOfHandStrategy == handStrategy.class

        where:
        handStrategyType            | classOfHandStrategy
        HandStrategiesType.ROCK     | RockHandStrategy.class
        HandStrategiesType.PAPER    | PaperHandStrategy.class
        HandStrategiesType.SCISSORS | ScissorsHandStrategy.class
        HandStrategiesType.RANDOM   | RandomHandStrategy.class
    }

    @Unroll
    def "Check polymorphism strategies types against hand decision"() {
        expect:
        HandStrategy handStrategy = handStrategiesFactory.getHandStrategy(handStrategyType)
        classOfHandStrategy == handStrategy.class
        handDecision == handStrategy.getDecision()

        where:
        handStrategyType            | classOfHandStrategy           |   handDecision
        HandStrategiesType.ROCK     | RockHandStrategy.class        |   HandDecisions.ROCK
        HandStrategiesType.PAPER    | PaperHandStrategy.class       |   HandDecisions.PAPER
        HandStrategiesType.SCISSORS | ScissorsHandStrategy.class    |   HandDecisions.SCISSORS

    }

}