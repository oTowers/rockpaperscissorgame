package com.oTowers.rockpaperscissorsgame.domain.model.rules

import com.oTowers.rockpaperscissorsgame.domain.model.HandDecisions
import com.oTowers.rockpaperscissorsgame.domain.model.rounds.RoundResults
import spock.lang.Specification
import spock.lang.Unroll

class RockPaperScissorsRulesTest extends Specification {

    private RockPaperScissorsRules rockPaperScissorsRules

    def setup() {
        rockPaperScissorsRules = new RockPaperScissorsRules()
    }

    @Unroll
    def "All combination Rock Paper Scissors Rules Test"() {
        expect:
        result == rockPaperScissorsRules.obtainWinner(playerOneDecision, playerTwoDecision)

        where:
        playerOneDecision       |   playerTwoDecision       |   result
        HandDecisions.PAPER     |   HandDecisions.PAPER     |   RoundResults.DRAW
        HandDecisions.ROCK      |   HandDecisions.PAPER     |   RoundResults.PLAYER_TWO_WIN
        HandDecisions.SCISSORS  |   HandDecisions.PAPER     |   RoundResults.PLAYER_ONE_WIN

        HandDecisions.PAPER     |   HandDecisions.ROCK      |   RoundResults.PLAYER_ONE_WIN
        HandDecisions.ROCK      |   HandDecisions.ROCK      |   RoundResults.DRAW
        HandDecisions.SCISSORS  |   HandDecisions.ROCK      |   RoundResults.PLAYER_TWO_WIN

        HandDecisions.PAPER     |   HandDecisions.SCISSORS  |   RoundResults.PLAYER_TWO_WIN
        HandDecisions.ROCK      |   HandDecisions.SCISSORS  |   RoundResults.PLAYER_ONE_WIN
        HandDecisions.SCISSORS  |   HandDecisions.SCISSORS  |   RoundResults.DRAW

    }
}
