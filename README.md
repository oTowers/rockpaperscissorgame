# RockPaperScissorsGame

Game Rock, Paper & Scissors

The basic rules are implemented:

* Rock beats Scissors
* Scissors beats Paper
* Paper beats Rock

## Usage
After you cloned the repository, you could build it with Maven
```
$ mvn clean package
```
and execute it with
```
$ java -jar target/rock-paper-scissors-game-0.0.1-SNAPSHOT.jar
```
the application starts a embedded webserver that listens on port 8080.

To play a round, you could open any tab of browser http://localhost:8080, and if you wish obtaind a reports of all games played, go to http://localhost:8080/report
